'use strict';

const gulp = require('gulp');
const del = require('del');
const fs = require('fs-extra');
const path = require('path');

const noPackageString = `# No Package Selected
Please select a document page on the left panel to view the documentation
`;

function createCountryDirs() {
  return fs.readdir('repos')
    .then((countries) => {
      return Promise.all(countries
        .map((country) => {
          return fs.ensureDir(path.join('src/assets/docs', country));
        }));
    });
}

function createCountryRepoDirs() {
  return fs.readdir('repos')
    .then((countries) => {
      return Promise.all(countries
        .map((country) => {
          return fs.readdir(path.join('repos', country))
            .then((repos) => {
              return Promise.all(repos
                .map((repo) => {
                  return fs.ensureDir(path.join('src/assets/docs', country, repo));
                }));
            });
        }));
    });
}

function cloneCountryRepoReadMe() {
  return fs.readdir('repos')
    .then((countries) => {
      return Promise.all(countries
        .map((country) => {
          return fs.readdir(path.join('repos', country))
            .then((repos) => {
              return Promise.all(repos
                .map((repo) => {
                  return fs.copyFile(path.join('repos', country, repo, 'README.md'), path.join('src/assets/docs', country, repo, 'README.md'));
                }));
            });
        }));
    });
}

function cloneCountryRepoPages() {
  return fs.readdir('repos')
    .then((countries) => {
      return Promise.all(countries
        .map((country) => {
          return fs.readdir(path.join('repos', country))
            .then((repos) => {
              return Promise.all(repos
                .map((repo) => {
                  return fs.readdir(path.join('repos', country, repo, 'docs'))
                    .then((docs) => {
                      return Promise.all(docs
                        .filter((doc) => {
                          return doc.includes('.md');
                        })
                        .map((doc) => {
                          return fs.copyFile(path.join('repos', country, repo, 'docs', doc), path.join('src/assets/docs', country, repo, doc));
                        }));
                    });
                }));
            });
        }));
    });
}

function createStaticPages() {
  return fs.ensureDir('src/assets/docs')
    .then(() => {
      return fs.writeFile('src/assets/docs/noPackageSelected.md', noPackageString);
    })
}

function generateDocJson() {
  const finalDoc = {};

  return fs.readdir('repos')
    .then((countries) => {
      return Promise.all(countries
        .map((country = '') => {
          finalDoc[country.toLowerCase()] = {
            country: country.toUpperCase(),
            packages: []
          };

          return fs.readdir(path.join('repos', country))
            .then((repos) => {
              return Promise.all(repos
                .map((repo) => {
                  return fs.readJson(path.join('repos', country, repo, 'package.json'))
                    .then((packageJson) => {
                      return fs.readdir(path.join('repos', country, repo, 'docs'))
                        .then((docs) => { return { packageJson, docs }});
                    })
                    .then((packageObj) => {
                      return finalDoc[country.toLowerCase()].packages.push({
                        name: packageObj.packageJson.name,
                        linkName: repo,
                        pages: packageObj.docs.slice(0)
                          .filter((doc) => {
                            return doc.includes('.md') && !doc.includes('README');
                          })
                          .map((doc) => {
                            return {
                              name: path.basename(doc, '.md'),
                              path: path.join('/docs', country, repo, path.basename(doc, '.md')).replace(/\\/g, '/').replace(/\s/g, '-'),
                              md: path.join('/assets/docs', country, repo, doc).replace(/\\/g, '/')
                            };
                          })
                          .sort((itemA, itemB) => {
                            if (itemA.name === 'Usage') {
                              return -1;
                            } else if (itemB.name === 'Usage') {
                              return 1;
                            }
                            return itemA.name.localeCompare(itemB.name);
                          })
                      });
                    });
                }));
            });
        }));
    })
    .then(() => {
      return fs.writeJson('src/assets/docs.json', finalDoc);
    });
}

function cleanDist() {
  return del('dist');
}

function cleanAssets() {
  return fs.writeJson('src/assets/docs.json', {})
    .then(() => { return del('src/assets/docs'); });
}

gulp.task('default', () => { return console.log('No default task')});

gulp.task('clean:dist', cleanDist);
gulp.task('build:docs', gulp.series(cleanAssets, createCountryDirs, createCountryRepoDirs, cloneCountryRepoReadMe, cloneCountryRepoPages, createStaticPages, generateDocJson));