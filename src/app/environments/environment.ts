interface AppEnvironmentVars {
  production: boolean;
}

export const environment: AppEnvironmentVars = {
  production: false
}
