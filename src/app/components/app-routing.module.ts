import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from './pages/home/home.component';
import { DocsPageComponent } from './pages/documentation/documentation.component';
import { DocsCountryPageComponent } from './pages/documentation/country.component';
import { LicensePageComponent } from './pages/license/license.component';
import { ExternalPageComponent } from './pages/external/external.component';

const routes: Routes = [
  { path: '', component: HomePageComponent, pathMatch: 'full' },
  { path: 'docs', component: DocsPageComponent },
  { path: 'license', component: LicensePageComponent },
  { path: 'external', component: ExternalPageComponent },
  { path: 'docs/:country', component: DocsCountryPageComponent },
  { path: 'docs/:country/:package', component: DocsCountryPageComponent },
  { path: 'docs/:country/:package/:page', component: DocsCountryPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
