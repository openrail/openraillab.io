import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Component({
  selector: 'license-page',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.scss']
})

export class LicensePageComponent {
  openRailLicenses: Array<object>;
  thirdPartyLicenses: Array<object>;

  constructor(private http: HttpClient) {
    this.openRailLicenses = [];
    this.thirdPartyLicenses = [];

    this.getJSON().subscribe(data => {
      this.openRailLicenses = (data.openRailLicenses || []).sort(this.sortLicenses);
      this.thirdPartyLicenses = (data.thirdPartyLicenses || []).sort(this.sortLicenses);
      console.log(data)
    });
  }

  public getJSON(): Observable<any> {
    return this.http.get('./assets/licenses.json')
  }

  private sortLicenses(itemA, itemB): number {
    return itemA.name.localeCompare(itemB.name);
  }
}
