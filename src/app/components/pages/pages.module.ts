import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http'; 

import {
  MatToolbarModule,
  MatSidenavModule,
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatExpansionModule,
  MatTabsModule
} from '@angular/material';
import { MarkdownModule } from 'ngx-markdown';

import { SharedModule } from '../shared/shared.module';

import { HomePageComponent } from './home/home.component';
import { DocsPageComponent } from './documentation/documentation.component';
import { DocsCountryPageComponent } from './documentation/country.component';
import { LicensePageComponent } from './license/license.component';
import { ExternalPageComponent } from './external/external.component';


@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatSidenavModule,
    SharedModule,
    MatButtonModule,
    RouterModule,
    MatIconModule,
    MatCardModule,
    MatExpansionModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient }),
    MatTabsModule
  ],
  declarations: [HomePageComponent, DocsPageComponent, DocsCountryPageComponent, LicensePageComponent, ExternalPageComponent],
  exports: [HomePageComponent, DocsPageComponent, DocsCountryPageComponent, LicensePageComponent, ExternalPageComponent]
})

export class PagesModule {}
