import { Component, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Component({
  selector: 'documentation-page',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss']
})

export class DocsPageComponent {
  docData: object;
  constructor(private http: HttpClient) {
    this.docData = {};

    this.getJSON().subscribe(data => {
      this.docData = data || {};
    });
  }

  public getJSON(): Observable<any> {
    return this.http.get('./assets/docs.json')
  }

  public getCountries() {
    return (this.docData && typeof this.docData === typeof {})
      ? Object.keys(this.docData)
        .map((country) => {
          return { key: country, code: this.docData[country].country };
        })
      : [];
  }
}