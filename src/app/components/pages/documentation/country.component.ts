import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { MarkdownService } from 'ngx-markdown';

@Component({
  selector: 'country-page',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})

export class DocsCountryPageComponent implements OnInit, OnDestroy {
  country: string;
  package: string;
  page: string;
  docData: object;
  sub: any;
  url: any;
  routerPath: string;

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.country = params['country'];
      this.package = params['package'];
      this.page = params['page'];
    });

    this.url = this.route.url.subscribe(segments => {
      this.routerPath = segments.slice(0)
        .reduce((fullUrl, currSegment) => {
          fullUrl = `${fullUrl}/${currSegment.path}`;
          return fullUrl;
        }, '');
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.url.unsubscribe();
  }

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient, private markdownService: MarkdownService) {
    const linkRenderer = this.markdownService.renderer.link;
    const htmlRenderer = this.markdownService.renderer.html;

    this.markdownService.renderer.link = (href, title, text) => {
      const html = linkRenderer.call(this.markdownService.renderer, href, title, text);

      return html
        .replace(/^<a href=\"https:\/\/openrail\.gitlab\.io(.+?)\"/i, `<a href=\"$1\"`)
        .replace(/^<a href=\"#(.+?)\"/i, `<a href=\"${this.routerPath}#$1\"`);
    };

    this.markdownService.renderer.html = (htmlRaw) => {
      const html = htmlRenderer.call(this.markdownService.renderer, htmlRaw);

      return html
        .replace(/<a href=\"https:\/\/openrail\.gitlab\.io(.+?)\"/gi, `<a href=\"$1\"`)
        .replace(/<a href=\"#(.+?)\"/gi, `<a href=\"${this.routerPath}#$1\"`);
    }

    this.docData = {};

    this.getJSON().subscribe(data => {
      this.docData = data || {};
    });
  }

  public getJSON(): Observable<any> {
    return this.http.get('./assets/docs.json')
  }

  private getCountryObj() {
    return (this.country && this.country !== '' && this.docData && this.docData[this.country] && this.docData[this.country].country)
      ? this.docData[this.country]
      : null;
  }

  public getCountry(): Observable<object> {
    return this.getCountryObj();
  }

  private getPackagesArr() {
    const countryObj = this.getCountryObj();

    return (countryObj && countryObj.packages && Array.isArray(countryObj.packages))
      ? countryObj.packages
      : [];
  }

  public getPackages(): Observable<object> {
    return this.getPackagesArr();
  }

  public getAssetPackageName(): string {
    const countryObj = this.getCountryObj();

    if (countryObj && countryObj.packages && Array.isArray(countryObj.packages)) {
      const packageArr = this.getPackagesArr();
      const packageObj = (this.package)
        ? packageArr.find(o => o.linkName === this.package)
        : null;

      if (packageArr && Array.isArray(packageArr) && this.package && packageObj) {
        const page = (this.page)
          ? packageObj.pages.find(o => o.name === this.page.replace(/-/g, ' '))
          : null;

        if (this.page && page) {
          return packageObj.name || '';;
        }
      }
    }
    return '';
  }

  private getAssetURL(): string {
    const countryObj = this.getCountryObj();

    if (countryObj && countryObj.packages && Array.isArray(countryObj.packages)) {
      const packageArr = this.getPackagesArr();
      const packageObj = (this.package)
        ? packageArr.find(o => o.linkName === this.package)
        : null;

      if (packageArr && Array.isArray(packageArr) && this.package && packageObj) {
        const page = (this.page)
          ? packageObj.pages.find(o => o.name === this.page.replace(/-/g, ' '))
          : null;

        if (this.page && page) {
          return page.md;
        } else {
          return `/assets/docs/${this.country}/${this.package}/README.md`;
        }
      }
    }
    return '/assets/docs/noPackageSelected.md';
  }

  public getPageURL(): string {
    return this.getAssetURL();
  }

  public goToUrl(url): void {
    console.log(url);
  }
}