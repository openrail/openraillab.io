import { Component } from '@angular/core';

@Component({
  selector: 'external-page',
  templateUrl: './external.component.html',
  styleUrls: ['./external.component.scss']
})

export class ExternalPageComponent {}
